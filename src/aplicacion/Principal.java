/* Copyright 2021 Carlos Alonso Gradillas
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.*/


package aplicacion;//metodo main

import mates.*;

	/**
	 *
	 * @author Carlos Alonso Gradillas
	 *
	 */

public class Principal{

	/**
	 * 
	 * En este metodo main se calcula el resultado final
	 *
	 * @param en args se establece un parametro para que el usuario meta el numero que quiera.
	 */


	public static void main(String[] args){
		long pasos = Long.parseLong(args[0]);
		Matematicas.generarNumeroPi(pasos);
	}//DesarolloPi metodo de la calse Pi.java
}












