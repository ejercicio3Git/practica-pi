/*
 * Copyright 2021 Carlos Alonso Gradillas
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.*/

package mates;

public class Matematicas{

	/**
	 *
	 * En esta clase se encuentra el metodo principal para generar el numero pi por el metodo de Montecarlo.
	 *
	 * @param pasos son todos los dardos que se lanzan
	 * 
	 */

	public static void generarNumeroPi(long pasos){
	
	double interior = 0;
	double areaCuadrado = 4;

	for(long i = 1; i<=pasos; i++){
	
		double a = Math.random()*2-1;
		double b = Math.random()*2-1;
		double numero = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
		if(numero<=1){
			interior++;
		}
	}

	double radio = 1;
	double areaCirculo = areaCuadrado*(interior/pasos);
	double aproximacionPi = areaCirculo/Math.pow(radio, 2);
	
	System.out.println("la aproximacion del numero Pi es "+ aproximacionPi);
	}
}















