* Copyright 2021 Carlos Alonso Gradillas
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

# README #



### Como funciona el programa ###

* La simulación de Montecarlo es un método estadístico. Este es utilizado para resolver problemas matemáticos complejos a través de la generación de variables aleatorias. A través de la simulación, se pueden resolver desde problemas muy sencillos, hasta problemas muy complejos. Algunos problemas pueden solucionarse con papel y bolígrafo. Sin embargo, la mayoría requieren el uso de programas informáticos.

* fuente https://economipedia.com/definiciones/simulacion-de-montecarlo.html

### Comandos para ejecutar programa ###

* 1. make compilar
* 2. make jar
* 3. java -jar numeroPi.jar numero_Punto_lanzar

### Autor del repositorio ###

* Carlos Alonso Gradillas
* 01/03/2021
